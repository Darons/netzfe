export class NewPost {
  
  title: string;
  description: string;
  category_id: number;
  image: any;
  deadline: string;
  avatar: string;
}