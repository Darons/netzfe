export class Post {

  constructor() {}


  id: number;
  avatar: string;
  title: string;
  description: string;
  category: string; //category name
  image: string; //may be null
  deadline: string; //date until an user could subscribe
  created_at: string;
  status: string; //availability to subscribe.
  like_number: number;
  comment_number: number;
  shared_number: number;
  is_subscribed: boolean; //if the user is suscribed.
  is_liked: boolean; //if the user has liked.
  is_shared: boolean; //if the post is original or shared.
  original_id: number;
  user: { //user data
    id: number,
    first_name: string,
    last_name: string,
    avatar: string,
    is_follower: boolean,
  }
  shared: { //user data of the user who shared.
    id: number,
    first_name: string,
    last_name: string
  }
}