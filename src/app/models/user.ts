export class User {

  constructor() {
    this.followers = this.following = this.participant_in = this.rating = 0;
  }
  
  avatar: string;
  profile_image: string;
  date_of_birth: string;
  degree: string;
  email: string;
  first_name: string;
  follower_list: any[];
  following_list: any[]; 
  followers: number;
  following: number;
  id: number;
  last_name: string;
  participant_in: number;
  phone: string;
  rating: number;
  uid: string;
}