export class Comment {

  id: 1;
  body: string;
  created_at: string;
  updated_at: string;
  user_id: number;
  user_fullname: string;

}