import { Component, OnInit, Input } from '@angular/core';
import { CommentComponent } from '../comment/comment.component';
import { UtilsService } from '../../core/utils.service';
import { PostPopoverComponent } from '../post-popover/post-popover.component';
import { environment } from '../../../environments/environment';
import { Post } from '../../models/post';
import { ActivityService } from '../../core/activity.service';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'netz-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {

  @Input() post: Post;
  readonly BASE_IMAGE_URL = environment.api;
  hasImage: boolean = false;
  readonly TODAY = new Date().toISOString().slice(0,10);
  date: string;
  dateType: boolean;
  icon_color: string;
  belongs: boolean;
  deleted: boolean = false;

  constructor(
    private util: UtilsService,
    private as: ActivityService,
    public popoverController: PopoverController
  ) { }

  ngOnInit() {
    this.date = new Date(this.post.created_at).toISOString().slice(0,10);
    this.dateType = new Date(this.date) < new Date(this.TODAY) ? true : false;
    this.icon_color = this.post.is_liked ? 'danger' : 'primary';
    let user = JSON.parse(localStorage.getItem('user'));
    let a = user.id;
    let b = this.post.user.id;
    this.belongs = a == b;
  }

  presentModal() {
    this.util.presentComponent('modal', CommentComponent, { id: this.post.id });
  }

  presentPopover(ev: any) {
    this.util.presentComponent('popover', PostPopoverComponent, ev, { id: this.post.id });
  }

  like(): void {
    if (!this.post.is_liked) {
      this.as.likeActivity(this.post.id).subscribe(_ => {
        this.post.is_liked = !this.post.is_liked;
        this.post.like_number++;
        this.icon_color = 'danger';
      }, _ => this.util.presentAlertError());
    } else {
      this.as.unlikeActivity(this.post.id).subscribe(_ => {
        this.post.is_liked = !this.post.is_liked;
        this.post.like_number--;
        this.icon_color = 'primary';
      }, _ => this.util.presentAlertError());
    }
  }

  share(): void {
    this.util.presentLoading();
    this.as.shareActivity(this.post.id).subscribe(_ => {
      this.util.dismissLoading();
      this.post.shared_number++;
      this.util.presentToast('Publicación compartida.');
    }, _ => {
      this.util.dismissLoading();
      this.util.presentAlertError();
    });
  }
}
