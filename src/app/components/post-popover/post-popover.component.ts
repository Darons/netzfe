import { Component, OnInit, Input } from '@angular/core';
import { ActivityService } from '../../core/activity.service';
import { AlertController, PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-post-popover',
  templateUrl: './post-popover.component.html',
  styleUrls: ['./post-popover.component.scss']
})
export class PostPopoverComponent implements OnInit {

  @Input() id: number;
  
  constructor(
    private as: ActivityService,
    public alertController: AlertController,
    public popoverController: PopoverController
  ) { }

  ngOnInit() {
  }

  delete(): void {
    this.presentAlertConfirm();
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      header: '¡Confirmar!',
      message: '¿Está seguro que desea eliminar?',
      cssClass: 'myalert',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            this.dismiss();
          }
        }, {
          text: 'Sí',
          handler: () => {
            this.as.delete(this.id).subscribe(_ => {});
            this.dismiss();
          }
        }
      ]
    });
    await alert.present();
  }

  dismiss():void {
    this.popoverController.dismiss();
  }
}
