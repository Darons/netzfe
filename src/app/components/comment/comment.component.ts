import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Comment } from '../../models/comment';
import { ActivityService } from '../../core/activity.service';
import { UtilsService } from '../../core/utils.service';

@Component({
  selector: 'netz-comment',
  templateUrl:'./comment.component.html',
  styleUrls: ['./comment.component.scss']
})
export class CommentComponent implements OnInit {

  comments: Array < Comment > = [];
  @Input() id: number;
  message: string;
  load: boolean;

  constructor(
    private modCtrl: ModalController,
    private as: ActivityService,
    private util: UtilsService
  ) {}

  ngOnInit() {
    this.load = true;
    this.message = '';
    this.as.getComments(this.id).subscribe(_ => {
      this.comments = _.comments;
      this.load = false;
    }, _ => {
      this.util.presentAlertError();
    });
  }

  dismiss(): void {
    this.modCtrl.dismiss();
  }
  
  comment(): void {
    this.as.createComment(this.id,  this.message).subscribe(_ => {
      this.comments = _.comments;
      this.message = '';
    }, _ => {
      this.util.presentAlertError();
    });
  }
}