import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'netz-noti',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {

  @Input() user: any;

  constructor() { }

  ngOnInit() {
  }

}
