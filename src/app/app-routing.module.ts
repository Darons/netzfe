import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AngularTokenService } from 'angular-token';

const routes: Routes = [
  {
    path: '', 
    loadChildren: './pages/tabs/tabs.module#TabsPageModule',
    canActivate: [AngularTokenService]
  },
  {
    path: 'login',
    loadChildren: './pages/login/login.module#LoginPageModule'
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: ''
  },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
