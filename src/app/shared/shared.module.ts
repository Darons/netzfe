import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostComponent } from '../components/post/post.component';
import { IonicModule } from '@ionic/angular';
import { UsersComponent } from '../components/users/users.component';
import { NotificationComponent } from '../components/notification/notification.component';
import { CommentComponent } from '../components/comment/comment.component';
import { PostPopoverComponent } from '../components/post-popover/post-popover.component';
import { ActivityService } from '../core/activity.service';
import { FormsModule } from '@angular/forms';
@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    FormsModule
  ],
  declarations: [
    PostComponent,
    UsersComponent,
    NotificationComponent,
    CommentComponent,
    PostPopoverComponent
  ],
  exports: [
    PostComponent,
    UsersComponent,
    NotificationComponent,
    CommentComponent
  ],
  providers: [
    ActivityService
  ]
})
export class SharedModule { }
