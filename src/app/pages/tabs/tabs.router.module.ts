import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';
import { HomePage } from '../home/home.page';
import { SearchPage } from '../search/search.page';
import { SubscriptionPage } from '../subscription/subscription.page';
import { NotificationPage } from '../notification/notification.page';
import { ProfilePage } from '../profile/profile.page';
import { ChatPage } from '../subscription/chat/chat.page';
import { EditProfileComponent } from '../profile/edit-profile/edit-profile.component';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: '',
        redirectTo: '/tabs/(home:home)',
        pathMatch: 'full',
      },
      {
        path: 'home',
        outlet: 'home',
        component: HomePage
      },
      {
        path: 'search',
        outlet: 'search',
        component: SearchPage
      },
      {
        path: 'subscription',
        outlet: 'subscription',
        component: SubscriptionPage
      },
      {
        path: 'chat',
        outlet: 'subscription',
        component: ChatPage
      },
      {
        path: 'notification',
        outlet: 'notification',
        component: NotificationPage
      },
      {
        path: 'profile',
        outlet: 'profile',
        component: ProfilePage
      },
      {
        path: 'edit',
        outlet: 'profile',
        component: EditProfileComponent
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/(home:home)',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
