import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TabsPageRoutingModule } from './tabs.router.module';
import { TabsPage } from './tabs.page';
import { HomePageModule } from '../home/home.module';
import { SearchPageModule } from '../search/search.module';
import { NotificationPageModule } from '../notification/notification.module';
import { ProfilePageModule } from '../profile/profile.module';
import { SubscriptionPageModule } from '../subscription/subscription.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    TabsPageRoutingModule,
    HomePageModule,
    SearchPageModule,
    NotificationPageModule,
    ProfilePageModule,
    SubscriptionPageModule
  ],
  declarations: [TabsPage]
})
export class TabsPageModule {}
