import { Component, OnInit } from '@angular/core';
import { Post } from '../../models/post';
import { ActivityService } from '../../core/activity.service';
import { environment } from '../../../environments/environment';
import { UserService } from '../../core/user.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {

  opt: string;
  users: any = [];
  posts: Array < Post > = [];
  query: string; 
  readonly BASE = environment.api;
  following: boolean[] = [];

  constructor(
    private as: ActivityService,
    private us: UserService
  ) {}

  ngOnInit(): void {
    this.opt = 'category';
  }

  search(ev: any): void {
    this.as.searchActivity(this.query, this.opt).subscribe(_ => {
    if (this.opt == 'category') {
      this.posts = _.activities;
      } else {
        this.users = _.users;
        this.users.forEach(_ => {
          this.following.push(true);
        });
    }
    });
  }

  follow(i: any, id: number): void {
    this.us.followUser(id).subscribe(_ => this.following[i] = false);
  }
}
