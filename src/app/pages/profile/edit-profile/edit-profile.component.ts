import { Component, OnInit } from '@angular/core';
import { User } from '../../../models/user';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { UserService } from '../../../core/user.service';
import { AuthService } from '../../../core/auth.service';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { UtilsService } from '../../../core/utils.service';
import { File, FileEntry } from "@ionic-native/file/ngx";
import { AndroidPermissions } from "@ionic-native/android-permissions/ngx";
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent implements OnInit {

  user: User = new User();
  passForm: FormGroup;
  readonly BASE_IMAGE = environment.api;
  cam;
  gal;
  displayImage: string;

  constructor(
    private us: UserService,
    private as: AuthService,
    private navCtrl: NavController,
    private util: UtilsService,
    private camera: Camera,
    private file : File,
    private androidPermissions: AndroidPermissions
  ) { 
    this.cam = camera.PictureSourceType.CAMERA;
    this.gal = camera.PictureSourceType.PHOTOLIBRARY;
  }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('user'));
    this.us.getUser(this.user.id).subscribe(res => {
      this.user = res.user;
    });

    this.passForm = new FormGroup({
      password: new FormControl('', [Validators.required, Validators.minLength(8)]),
      passwordConfirmation: new FormControl('', [Validators.required, Validators.minLength(8)]),
      passwordCurrent: new FormControl('', [Validators.required, Validators.minLength(8)])
    });

    this.displayImage = this.user.avatar == null ? 'assets/img/nopic.png' : this.BASE_IMAGE + this.user.avatar;
  }

  dismiss(): void {
    this.navCtrl.navigateBack('tabs/(profile:profile)');
  }

  updateProfile(): void {
    this.us.updateUser(this.user).subscribe(_ => {
      this.dismiss();
    }, e => this.util.presentAlert(e));
  }

  changePassword(): void {
    if (this.passForm.valid) {
      this.as.changePassword({
        password: this.password.value,
        passwordConfirmation: this.passwordConfirmation.value,
        passwordCurrent: this.passwordCurrent.value
      }).subscribe(_ => {
        this.util.presentToast('Contraseña actualizada.');
        this.dismiss();
      }, e => {
        this.util.presentToast(e.error);        
      });

    } else {
      this.util.presentToast('La contraseña debe tener mínimo 8 caracteres.')
    }
  }

  async openCamera(source: number) {
    this.util.presentLoading();
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: source,
      targetWidth: 250,
      targetHeight: 250
    }
    try {
      let cameraInfo = await this.camera.getPicture(options);
      this.user.avatar = this.user.profile_image = 'data:image/jpeg;base64,' + cameraInfo;
      this.displayImage = this.user.avatar;
    } catch (e) {
      this.util.presentAlert(JSON.stringify(e));
    }
    this.util.dismissLoading();
  }

  /*makeFileIntoBlob(_imagePath) {
    // INSTALL PLUGIN - cordova plugin add cordova-plugin-file
    return new Promise((resolve, reject) => {
      let fileName = "";
      this.file.resolveLocalFilesystemUrl(_imagePath).then(fileEntry => {
          let { name, fullPath } = fileEntry;

          // get the path..
          let path = fullPath

          fileName = name;
          this.util.presentAlert(path+name);
          // we are provided the name, so now read the file into
          // a buffer
          return this.file.readAsArrayBuffer(path, name);
        })
        .then(buffer => {
          // get the buffer and make a blob to be saved
          let imgBlob = new Blob([buffer], {
            type: "image/jpeg"
          });
          console.log(imgBlob.type, imgBlob.size);
          resolve({
            fileName,
            imgBlob
          });
        })
        .catch(e => {
          this.util.presentAlert(JSON.stringify(e));
          reject(e)
        });
    });
  }*/


  /*makeFileIntoBlob(_imagePath) {
    return new Promise((resolve, reject) => {

      this.file.resolveLocalFilesystemUrl(_imagePath).then((fileEntry: FileEntry) => {
        fileEntry.file((resFile) => {
          reader.onloadend = (evt: any) => {
            var imgBlob: any = new Blob([evt.target.result], {
              type: "image/jpeg"
            });
            imgBlob.name = 'sample.jpg';
            resolve(imgBlob);
          };
          reader.onerror = (e) => {
            reject(e);
          };
          reader.readAsArrayBuffer(resFile);
        });
      });
    });
  }*/

  get password(): AbstractControl {
    return this.passForm.get('password');
  }
  get passwordConfirmation(): AbstractControl {
    return this.passForm.get('passwordConfirmation');
  }
  get passwordCurrent(): AbstractControl {
    return this.passForm.get('passwordCurrent');
  }
}
