import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { UserService } from '../../../core/user.service';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-follow',
  templateUrl: './follow.component.html',
  styleUrls: ['./follow.component.scss']
})
export class FollowComponent implements OnInit {

  @Input() type: number;
  @Input() data: any[];
  readonly BASE = environment.api;
  constructor(
    private modCtrl: ModalController,
    private us: UserService
  ) { 
  }

  ngOnInit() {
  }

  dismiss(): void {
    this.modCtrl.dismiss();
  }

  gotoProfile(): void {
    console.log('ohboy')
  }

  delete(user: any): void {
    if (this.type == 1) {
      return;
    }
    console.log(this.data)
    this.us.unfollowUser(user['id']).subscribe(_ => { 
      this.data = this.data.filter(__ => __.id != _.user.id);
    });
  }
}

