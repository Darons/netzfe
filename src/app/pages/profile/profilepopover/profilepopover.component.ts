import { Component, OnInit } from '@angular/core';
import { PopoverController, NavController } from '@ionic/angular';
import { AuthService } from '../../../core/auth.service';

@Component({
  selector: 'app-profilepopover',
  templateUrl: './profilepopover.component.html',
  styleUrls: ['./profilepopover.component.scss']
})
export class ProfilepopoverComponent implements OnInit {

  constructor(
    public popoverController: PopoverController,
    private auth: AuthService,
    private navCtrl: NavController
  ) { }

  ngOnInit() {
  }

  signUp(): void {
    this.auth.signOut().subscribe(res => {
      this.popoverController.dismiss();
      this.dismiss();
      localStorage.removeItem('id');
      this.navCtrl.navigateRoot('login');
    });
  }

  dismiss(): void {
    this.popoverController.dismiss();
  }
}

