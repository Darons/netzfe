import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ProfilePage } from './profile.page';
import { SharedModule } from '../../shared/shared.module';
import { UserService } from '../../core/user.service';
import { HttpClientModule } from '@angular/common/http';
import { ProfilepopoverComponent } from './profilepopover/profilepopover.component';
import { AuthService } from '../../core/auth.service';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { FollowComponent } from './follow/follow.component';
import { UtilsService } from '../../core/utils.service';
import { ActivityService } from '../../core/activity.service';

const routes: Routes = [
  {
    path: '',
    component: ProfilePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    SharedModule,
    HttpClientModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    ProfilePage, 
    ProfilepopoverComponent, 
    EditProfileComponent, FollowComponent
  ],
  providers: [
    UserService,
    AuthService,
    UtilsService
  ],
  entryComponents: [
    ProfilepopoverComponent,
    FollowComponent
  ]
})
export class ProfilePageModule {}
