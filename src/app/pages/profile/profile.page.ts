import { Component, OnInit } from '@angular/core';
import { Post } from '../../models/post';
import { UserService } from '../../core/user.service';
import { ProfilepopoverComponent } from './profilepopover/profilepopover.component';
import { FollowComponent } from './follow/follow.component';
import { User } from '../../models/user';
import { Subscription } from 'rxjs';
import { UtilsService } from '../../core/utils.service';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  posts: Array < Post > = new Array < Post > ();
  page: number;
  user: User = new User();
  sub: Subscription[] = [];
  readonly BASE_IMAGE = environment.api;

  constructor(
    private us: UserService,
    private util: UtilsService
  ) {}

  /**
   * Lifecycle hook that initialize the view fetching data from the server.
   */
  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('user'));
    let a = this.us.getUser(this.user.id).subscribe(res => {
      this.user = res.user;
    }, e => this.util.presentAlertError());
    this.page = 1;
    let b = this.us.getActivities(this.user.id, this.page).subscribe(_ => {
      this.posts = _.activities;
    });

    this.sub.push(a,b);
  }

  /**
   * Lifecycle hook that updates the view after routing back from forward pages.
   */
  ionViewDidEnter(): void {
    this.ngOnInit();
  }

  /**
   * Lifecycle hook that shut down the subscriptions.
   */
  ionViewDidLeave(): void {
    this.sub.forEach(_ => _.unsubscribe());
  }
  
  /**
   * 
   * @param infiniteScroll Event emitted when treshold distance is reached.
   */
  doInfinite(infiniteScroll) {
    setTimeout(() => {
      this.page++;
      this.sub.push(this.us.getActivities(this.user.id, this.page).subscribe(_ => this.posts = [...this.posts,..._.activities]));
      infiniteScroll.target.complete();
    }, 500);
  }

  presentPopover(ev: any) {
    this.util.presentComponent('popover', ProfilepopoverComponent, ev);
  }

  presentModal(type: number) {
    let prop = { 
      type: type,
      data: type == 1 ? this.user.follower_list : this.user.following_list
    };
    this.util.presentComponent('modal', FollowComponent, prop);
  }
}
