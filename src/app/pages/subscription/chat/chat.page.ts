import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit {

  messages: { text: string, time: string }[] = [];
  time1: string;
  time2: string;
  message: string;

  constructor() { }

  ngOnInit() {
    this.message = '';
    this.time1 = new Date(new Date().getTime() - 35*60*1000).toLocaleTimeString();
    this.time2 = new Date(new Date().getTime() - 23*60*1000).toLocaleTimeString();
  }

  sendMessage():void {
    this.messages.push({ text: this.message, time: new Date().toLocaleTimeString() });
    this.message = '';
  }

}
