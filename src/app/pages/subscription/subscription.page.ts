import { Component, OnInit } from '@angular/core';
import { Post } from '../../models/post';
import { UserService } from '../../core/user.service';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-subscription',
  templateUrl: './subscription.page.html',
  styleUrls: ['./subscription.page.scss'],
})
export class SubscriptionPage implements OnInit {

  posts: Array < Post > = []
  readonly BASE = environment.api;
  
  constructor(
    private us: UserService
  ) {}

  ngOnInit() {
    this.us.getActivities(JSON.parse(localStorage.getItem('user')).id, 1).subscribe(_ => this.posts = _.activities);
  }
}