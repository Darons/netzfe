import { Component, OnInit } from '@angular/core';
import { Post } from '../../models/post';
import { ModalController } from '@ionic/angular';
import { PostPage } from './post/post.page';
import { UtilsService } from '../../core/utils.service';
import { ActivityService } from '../../core/activity.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage implements OnInit {

  posts: Array < Post > = [];
  page: number;
  sub: Array < Subscription > = [];

  constructor(
    private util: UtilsService,
    private as: ActivityService,
    public modalController: ModalController
  ) {}

  ngOnInit(): void {
    this.page = 1;
    this.sub.push(this.as.getActivities(this.page).subscribe(_ => {
      this.posts = _.activities;
    }, _ => {
      this.util.presentAlertError()
    }));
  }

  async presentModal() {
    const modal = await this.modalController.create({
    component: PostPage
    });
  
    await modal.present();
  
    await modal.onWillDismiss().then(_ => this.ngOnInit());  
  }

  doRefresh(event) {
    setTimeout(() => {
      this.page = 1;
      this.sub.push(this.as.getActivities(this.page).subscribe(_ => this.posts = [..._.activities],
        _ => this.util.presentAlertError()));
      event.target.complete();
    }, 2000);
  }

  doInfinite(infiniteScroll) {
    setTimeout(() => {
      this.page++;
      this.sub.push(this.as.getActivities(this.page).subscribe(_ => this.posts = [...this.posts,..._.activities],
        _ => this.util.presentAlertError()));
      infiniteScroll.target.complete();
    }, 500);
  }

}