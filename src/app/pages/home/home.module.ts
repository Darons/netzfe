import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomePage } from './home.page';
import { SharedModule } from '../../shared/shared.module';
import { PostPage } from './post/post.page';
import { CommentComponent } from '../../components/comment/comment.component';
import { ActivityService } from '../../core/activity.service';
import { UtilsService } from '../../core/utils.service';
import { PostPopoverComponent } from '../../components/post-popover/post-popover.component';
import { CategoryService } from '../../core/category.service';

const routes = [
  { path: '', component: HomePage },
  { path: 'post', component: PostPage }
]

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    HomePage,
    PostPage
  ],
  providers: [
    ActivityService,
    UtilsService,
    DatePipe,
    CategoryService
  ],
  entryComponents: [
    CommentComponent,
    PostPopoverComponent
  ]
})
export class HomePageModule {}
