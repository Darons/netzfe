import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ActivityService } from '../../../core/activity.service';
import { environment } from '../../../../environments/environment';
import { Category } from '../../../models/category';
import { User } from '../../../models/user';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { CategoryService } from '../../../core/category.service';
import { UtilsService } from '../../../core/utils.service';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { NewPost } from '../../../models/new-post';

@Component({
  selector: 'app-post',
  templateUrl: './post.page.html',
  styleUrls: ['./post.page.scss'],
})
export class PostPage implements OnInit {

  readonly BASE_IMAGE_URL = environment.api;
  categories: Array < Category > = [];
  user: User = new User();
  postForm: FormGroup;
  readonly TODAY = new Date().toISOString().slice(0, 10); 
  image: any = null;
  cam;
  gal;

  constructor(
    private modCtrl: ModalController,
    private cs: CategoryService,
    private as: ActivityService,
    private camera: Camera,
    private util: UtilsService
  ) {
    this.cam = camera.PictureSourceType.CAMERA;
    this.gal = camera.PictureSourceType.PHOTOLIBRARY;
  }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('user'));
    
    this.postForm = new FormGroup({
      'title': new FormControl('',[Validators.required]),
      'description': new FormControl('',[Validators.required]),
      'category_id': new FormControl('',[Validators.required]),
      'deadline': new FormControl('',[Validators.required])
    });

    this.cs.getCategories().subscribe(_ => this.categories = _.categories, _ => this.util.presentAlertError());
  }

  dismiss(): void {
    this.modCtrl.dismiss();
  }

  async openCamera(source: number) {
    this.util.presentLoading();
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: source,
      targetWidth: 250,
      targetHeight: 250
    }
    try {
      let cameraInfo = await this.camera.getPicture(options);
      this.image = 'data:image/jpeg;base64,' + cameraInfo;
      
    } catch (e) {
      this.util.presentAlert(JSON.stringify(e));
    }
    this.util.dismissLoading();
  }

  post(): void {
    if (this.postForm.valid) {
      let post = new NewPost();
      post.title = this.title.value;
      post.deadline = this.deadline.value;
      post.category_id = this.category_id.value;
      post.description = this.description.value;
      post.avatar = this.image;
      this.as.createActivity(post).subscribe(_ => {
        this.dismiss();
      }, e => this.util.presentAlertError());
    }
  }

  get title(): AbstractControl {
    return this.postForm.get('title');
  }

  get description(): AbstractControl {
    return this.postForm.get('description');
  }

  get category_id(): AbstractControl {
    return this.postForm.get('category_id');
  }

  get deadline(): AbstractControl {
    return this.postForm.get('deadline');
  }
}
