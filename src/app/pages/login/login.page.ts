import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { AuthService } from '../../core/auth.service';
import { UtilsService } from '../../core/utils.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  selectedSegment: string;
  upForm: FormGroup;
  inForm: FormGroup;
  user: any = {};
  logIn: any = {};

  constructor(
    private navCtrl: NavController,
    private auth: AuthService,
    private util: UtilsService
  ) { }

  /**
   * Lifecycle hook that initialize the view.
   */
  ngOnInit(): void {
    this.selectedSegment = 'signin';

    this.upForm = new FormGroup({
      'email': new FormControl('', [Validators.required, Validators.email]),
      'password': new FormControl('', [Validators.required, Validators.minLength(8)]),
      'passwordConfirmation': new FormControl('', [Validators.required]),
      'firstName': new FormControl('', [Validators.required]),
      'lastName': new FormControl('', [Validators.required]),
      'birthDate': new FormControl('', [Validators.required])
    });
    this.inForm = new FormGroup({
      'email': new FormControl('', [Validators.required]),
      'password': new FormControl('', [Validators.required, Validators.minLength(8)])
    });
  }

  /**
   * Register the user.
   */
  signUp(): void {
    if (this.upForm.valid) {
      this.user['login'] = this.emailUp.value;
      this.user['password'] = this.passwordUp.value;
      this.user['password_confirmation'] = this.confirmation.value;
      this.user['first_name'] = this.firstName.value;
      this.user['last_name'] = this.lastName.value;
      this.user['date_of_birth'] = this.birthDate.value;
      this.auth.signUp(this.user).subscribe(res => {
        this.util.presentToast('Usuario registrado.');
        this.inForm.reset();
        this.selectedSegment = 'signin';
      }, e => {
        let message = e.status == 422 ? 'El correo ya fue registrado.' : 'Error de conexión con el servidor.';
        this.util.presentToast(message);
      });
    }
  }

  /**
   * Register the user.
   */
  signIn(): void {
    if(this.inForm.valid) {
      this.util.presentLoading();
      this.logIn['login'] = this.emailIn.value;
      this.logIn['password'] = this.passwordIn.value;
      this.auth.signIn(this.logIn).subscribe(res => {
        let user = JSON.stringify(res.body.data);
        localStorage.setItem('user', user);
        this.navCtrl.navigateRoot('');
        this.util.dismissLoading();
      }, e => {
        let message = e.status == 401 ? 'Usuario o contraseña incorrectos.' : 'Error de conexión con el servidor.';
        this.util.dismissLoading();
        this.util.presentToast(message);        
      });
    }
  }

  /**
   * FormControl getters form easy accessability.
   */
  get emailUp(): AbstractControl {
    return this.upForm.get('email');
  }
  get emailIn(): AbstractControl {
    return this.inForm.get('email');
  }
  get passwordIn(): AbstractControl {
    return this.inForm.get('password');
  }
  get passwordUp(): AbstractControl {
    return this.upForm.get('password');
  }
  get confirmation(): AbstractControl {
    return this.upForm.get('passwordConfirmation');
  }
  get firstName(): AbstractControl {
    return this.upForm.get('firstName');
  }
  get lastName(): AbstractControl {
    return this.upForm.get('lastName');
  }
  get birthDate(): AbstractControl {
    return this.upForm.get('birthDate');
  }
}
