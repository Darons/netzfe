import { Component, OnInit } from '@angular/core';
import { User } from '../../models/user';
import { UserService } from '../../core/user.service';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.page.html',
  styleUrls: ['./notification.page.scss'],
})

export class NotificationPage implements OnInit {

  user: User = new User;

  constructor(
    private us: UserService
  ) {}

  ngOnInit(): void {
    this.us.getUser(JSON.parse(localStorage.getItem('user')).id).subscribe(_ => this.user = _.user);
  }
}