import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { NotificationPage } from './notification.page';
import { SharedModule } from '../../shared/shared.module';
import { UserService } from '../../core/user.service';

const routes: Routes = [
  {
    path: '',
    component: NotificationPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  declarations: [
    NotificationPage
  ],
  providers: [
    UserService
  ]
})
export class NotificationPageModule {}
