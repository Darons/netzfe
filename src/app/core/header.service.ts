import { HttpHeaders } from '@angular/common/http';
import { AngularTokenService } from 'angular-token';

export class HeaderService {

  header: HttpHeaders = new HttpHeaders();
  
  /**
   * Set headers for requests.
   */
  constructor(
    tokenService: AngularTokenService
  ) {
    let a = tokenService.currentAuthData;
    if (a != undefined) {
      this.header = this.header.set('access-token', a.accessToken);
      this.header = this.header.set('client', a.client);
      this.header = this.header.set('expiry', a.expiry);
      this.header = this.header.set('token-type', a.tokenType);
      this.header = this.header.set('uid', a.uid);
    }
  }
}
