import { Injectable } from '@angular/core';
import { HeaderService } from './header.service';
import { AngularTokenService } from 'angular-token';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { User } from '../models/user';

@Injectable()
export class UserService {

  userUrl: string = environment.api + '/users/';
  authUrl: string = environment.api + '/auth';

  constructor(
    private tokenService: AngularTokenService,
    private http: HttpClient
    ) {}

  /**
   * Return user given ID.
   * @param id User ID.
   */
  getUser(id: number): Observable < any > {
    const header = new HeaderService(this.tokenService).header;
    const url = this.userUrl + id.toString();
    return this.http.get(url, {
      headers: header
    });
  }

  /**
   * Update an user.
   * @param user User data to update.
   */
  updateUser(user: User): Observable < any > {
    const header = new HeaderService(this.tokenService).header;
    return this.http.patch(this.authUrl, user, {
      headers: header
    });
  }

  /**
   * Follow an user given ID.
   * @param id User ID to follow.
   */
  followUser(id: number): Observable < any > {
    const header = new HeaderService(this.tokenService).header;
    const url = this.userUrl + id.toString() + '/follow';
    return this.http.post(url, {}, {
      headers: header
    });
  }

  /**
   * Follow an user given ID.
   * @param id User ID to follow.
   */
  unfollowUser(id: number): Observable < any > {
    const header = new HeaderService(this.tokenService).header;
    const url = this.userUrl + id.toString() + '/unfollow';
    return this.http.post(url, {}, {
      headers: header
    });
  }

  /**
   * Return all the activities created and shared by the user.
   * @param id User ID.
   */
  getActivities(id: number, page: number): Observable < any > {
    const param = new HttpParams().append('page', page.toString());
    const header = new HeaderService(this.tokenService).header;
    const url = this.userUrl + id.toString() + '/activities';
    return this.http.get(url, {
      headers: header,
      params: param
    });
  }
}
