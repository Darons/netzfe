import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AngularTokenService } from 'angular-token';
import { Observable } from 'rxjs';
import { HeaderService } from './header.service';
import { NewPost } from '../models/new-post';

@Injectable()
export class ActivityService {

  url: string = environment.api + '/activities/';

  constructor(
    private http: HttpClient,
    private tokenService: AngularTokenService,
  ) {}

  /**
   * Return an activity.
   * @param id Activity id.
   */
  getActivity(id: number): Observable < any > {
    const header = new HeaderService(this.tokenService).header;
    return this.http.get(this.url + id.toString(), {
      headers: header
    });
  }

  /**
   * Return activities for home.
   * @param page: Page number.
   */
  getActivities(page: number): Observable < any > {
    const param = new HttpParams().append('page', page.toString());
    const header = new HeaderService(this.tokenService).header;
    return this.http.get(this.url, {
      headers: header,
      params: param
    });
  }

  /**
   * Search activities matchin the query.
   * @param query Query to search.
   */
  searchActivity(query: string, type: string): Observable < any > {
    const param = new HttpParams().append('query', query);
    const header = new HeaderService(this.tokenService).header;
    return this.http.get(environment.api + '/search/search_' + type, {
      headers: header,
      params: param
    });
  }

  /**
   * Delete an activity.
   * @param id Activity id.
   */
  delete(id: number): Observable < any > {
    const header = new HeaderService(this.tokenService).header;
    return this.http.delete(this.url + id.toString(), {
      headers: header
    });
  }

  /**
   * Create an activity.
   * @param body Activity data.
   */
  createActivity(body: NewPost): Observable < any > {
    const header = new HeaderService(this.tokenService).header;
    return this.http.post(this.url, body, {
      headers: header
    });
  }

  /**
   * Update an activity.
   * @param body Activity data.
   */
  updateActivity(body: NewPost): Observable < any > {
    const header = new HeaderService(this.tokenService).header;
    return this.http.patch(this.url, body, {
      headers: header
    });
  }

  /**
   * Return activity comments.
   * @param number Activity id.
   */
  getComments(id: number): Observable < any > {
    const header = new HeaderService(this.tokenService).header;
    return this.http.get(this.url + id.toString() + '/comments', {
      headers: header
    });
  }

  /**
   * Create a comment.
   * @param id Activity id.
   * @param message Comment
   */
  createComment(id: number, message: string): Observable < any > {
    const header = new HeaderService(this.tokenService).header;
    return this.http.post(this.url + id.toString() + '/comments', { body : message }, {
      headers: header
    });
  }

  /**
   * Like an activity.
   * @param id Activity id.
   */
  likeActivity(id: number): Observable < any > {
    const header = new HeaderService(this.tokenService).header;
    return this.http.post(this.url + id.toString() + '/like', {}, {
      headers: header
    });
  }

  /**
   * Unlike an activity.
   * @param id Activity id.
   */
  unlikeActivity(id: number): Observable < any > {
    const header = new HeaderService(this.tokenService).header;
    return this.http.post(this.url + id.toString() + '/unlike', {}, {
      headers: header
    });
  }

  /**
   * Rate an user.
   * @param id Activity id.
   */
  rateUser(id: number, user: number, rate: number): Observable < any > {
    const header = new HeaderService(this.tokenService).header;
    return this.http.post(this.url + id.toString() + '/voteup', {
      votable_user_id: user,
      vote_weight: rate
    }, {
      headers: header
    });
  }

  shareActivity(id: number): Observable < any > {
    const header = new HeaderService(this.tokenService).header;
    return this.http.post(this.url + id.toString() + '/share', {}, {
      headers: header
    });
  }
}


