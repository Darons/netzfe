import { Injectable } from '@angular/core';
import { HeaderService } from './header.service';
import { AngularTokenService, RegisterData, SignInData, UpdatePasswordData } from 'angular-token';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AuthService extends HeaderService {

  constructor(
    private tokenService: AngularTokenService,
    ) {
    super(tokenService);
   }

   /**
    * Register a new user.
    * @param user User data.
    */
   signUp(user: RegisterData): Observable < any > {
     return this.tokenService.registerAccount(user);
   }

   /**
    * Sign in the user.
    * @param user e-mail and password
    */
   signIn(user: SignInData): Observable < any > {
     return this.tokenService.signIn(user);
   }

   /**
    * Sign out the user.
    */
   signOut(): Observable < any > {
     return this.tokenService.signOut();
   }

   /**
    * 
    * @param data New, current and confirmation.
    */
   changePassword(data: UpdatePasswordData): Observable < any > {
     return this.tokenService.updatePassword(data);
   }
}
