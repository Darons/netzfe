import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { AngularTokenService } from 'angular-token';
import { Observable } from 'rxjs';
import { HeaderService } from './header.service';

@Injectable()
export class CategoryService {

  url: string = environment.api + '/categories';

  constructor(
    private tokenService: AngularTokenService,
    private http: HttpClient
  ) {}

  getCategories(): Observable < any > {
    const header = new HeaderService(this.tokenService).header;
    return this.http.get(this.url, {
      headers: header
    });
  }
}
