import { Injectable } from '@angular/core';
import { ToastController, ModalController, PopoverController, AlertController, LoadingController } from '@ionic/angular';

@Injectable()
export class UtilsService {

  constructor(
    public toastController: ToastController,
    public modalController: ModalController,
    public popoverController: PopoverController,
    public alertController: AlertController,
    public loadingController: LoadingController
  ) {}

  /**
   * Present a toast message.
   * @param message Message to show in the toast.
   */
  async presentToast(message : string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 3000,
      color: 'primary'
    });
    toast.present();
  }

  /**
   * Present a modal or popover component.
   * @param type Type of component to show, may be 'modal' or 'popover'
   * @param component Instance of component to show.
   * @param ev Event handler for popover or props for modal.
   * @param cp props for popover.
   */
  async presentComponent(type: string, component: any, ev? : any, cp?: any) {
    switch (type) {
      case 'modal':
        const modal = await this.modalController.create({
          component: component,
          componentProps: ev
        });
        return await modal.present();

      case ('popover'):
        const popover = await this.popoverController.create({
          component: component,
          event: ev,
          translucent: false,
          componentProps: cp
        });
        await popover.present();
        break;
    }
  }

  /**
   * Alert error for backend problems.
   */
  async presentAlertError() {
    const message = 'Al parecer nuestros servidores están echando humo. Regresa en unos minutos mientras manejamos la situación.';
    const alert = await this.alertController.create({
      header: '¡Oops!',
      message: message,
      cssClass: 'myalert',
      buttons: [
        {
          text: 'OK',
          cssClass: 'secondary',
          handler: () => {
          }
        }
      ]
    });
    await alert.present();
  }

  /**
   * Alert for error.
   * @param message: Message to display in the alert. 
   */
  async presentAlert(message: string) {
    const alert = await this.alertController.create({
      header: 'Error',
      message: message,
      cssClass: 'myalert',
      buttons: [
        {
          text: 'OK',
          cssClass: 'secondary'
        }
      ]
    });
    await alert.present();
  }

  /**
   * Loading screen handlers
   */
  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'Cargando...',
    });
    await loading.present();
  }

  dismissLoading(): void {
    this.loadingController.dismiss();
  }
}
